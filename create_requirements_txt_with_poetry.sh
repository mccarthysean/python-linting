#!/bin/bash

# This script creates a requirements.txt file with poetry

# Exit immediately if a command exits with a non-zero status
set -e

echo -e "\nCreating requirements.txt file with poetry"

poetry export -f requirements.txt --without-hashes --output requirements.txt

echo -e "\nrequirements.txt file created successfully"
